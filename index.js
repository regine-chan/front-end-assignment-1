// Caches every html element used
const Balance = document.getElementById("Balance");
const Loan = document.getElementById("Loan");
const LoanButton = document.getElementById("LoanButton");
const Pay = document.getElementById("Pay");
const BankButton = document.getElementById("BankButton");
const WorkButton = document.getElementById("WorkButton");
const LaptopsInput = document.getElementById("LaptopsInput");
const Features = document.getElementById("Features");
const ComputerImage = document.getElementById("ComputerImage");
const ComputerName = document.getElementById("ComputerName");
const ComputerDescription = document.getElementById("ComputerDescription");
const ComputerPrice = document.getElementById("ComputerPrice");
const BuyButton = document.getElementById("BuyButton");
const MyComputerImage = document.getElementById("MyComputerImage");
const MyComputerName = document.getElementById("MyComputerName");
const MyComputerDescription = document.getElementById("MyComputerDescription");
const MyComputers = document.getElementById("MyComputers");
const PreviousButton = document.getElementById("Previous");
const NextButton = document.getElementById("Next");
const DownPaymentButton = document.getElementById("DownPaymentButton");

// Initializes a person object
const person = {
    name: "Joe Banker",
    pay: 0,
    balance: 20000,
    loan: 0,
    computers: [],
    doWork: function () {
        this.pay += 100;
        Pay.innerText = this.pay;
    },
    doLoan: function () {
        if (this.loan > 0) {
            alert("Cant loan anymore");
        } else {
            try {
                let amount = parseInt(prompt("Please enter loan amount", 10000));
                if (typeof amount == "number") {
                    if (amount <= (this.balance * 2)) {
                        this.balance += amount;
                        Balance.innerText = this.balance;
                        this.loan += amount;
                        Loan.innerText = this.loan;
                    } else {
                        alert("TOOOOO MUCH");
                    }
                }
            } catch (any) {
                alert("Wrong type");
            }
        }
    },
    doBuy: function () {
        if (this.balance > selectedLaptop.price) {
            this.computers.push(selectedLaptop);
            this.balance -= selectedLaptop.price;
            Balance.innerText = this.balance;
            MyComputers.hidden = false;
            displayMyComputers(this.computers.length - 1);
        } else {
            alert("Not enuff money");
        }
    },
    doBank: function () {
        this.balance += this.pay;
        this.pay = 0;
        Balance.innerText = this.balance;
        Pay.innerText = this.pay;
    },
    doPayDownLoan: function (amount) {
        if (this.balance > amount) {
            this.loan -= amount;
            this.balance -= amount;
            Balance.innerText = this.balance;
            Loan.innerText = this.loan;
        } else {
            alert("You don't have that kind of money!");
        }
    }
}

// Initializes selectors to hold indexes / objects for reference 
let selectedLaptop;
let selectedMyLaptop;

// Laptop class
function Laptop(imageUrl, name, description, price, features) {
    this.imageUrl = imageUrl;
    this.name = name;
    this.description = description;
    this.price = price;
    this.features = features;
    this.display = function () {
        ComputerImage.src = this.imageUrl;
        ComputerName.innerText = this.name;
        ComputerDescription.innerText = this.description;
        ComputerPrice.innerText = this.price;
        Features.innerText = this.features;
    }
};

// Displays a computer owned by person using index
function displayMyComputers(index) {
    selectedMyLaptop = index;
    MyComputerImage.src = person.computers[index].imageUrl;
    MyComputerName.innerText = person.computers[index].name;
    MyComputerDescription.innerText = person.computers[index].description;
}

// Initializes laptops
const laptops = [];
laptops.push(
    new Laptop(
        "./laptop1.jpeg",
        "Samsung Galaxy Book S 13",
        "Samsung Galaxy Book S bærbar PC kombinerer en svært lett vekt og portabilitet med en høytytende brikke som leverer avansert ytelse under krevende oppgaver. LED-skjermen byr på presis fargegjengivelse slik at du kan jobbe hvor som helst.",
        14795,
        "Intel® Core™ i5-L16G7-prosessor \n8 GB LDDR4x RAM \n512 GB eUFS-flashminne"
    ));
laptops.push(
    new Laptop(
        "./laptop2.jpeg",
        "Samsung Galaxy Book Ion 13,3",
        "Samsung Galaxy Book ION bærbar PC kombinerer en svært lett vekt og portabilitet med en høytytende brikke som leverer avansert ytelse under krevende oppgaver. QLED-skjermen byr på presis fargegjengivelse slik at du kan jobbe hvor som helst.",
        16995,
        "Intel® Core™ i7-10510U-prosessor \n16 GB DDR4 RAM \n512 GB PCIe SSD"
    ));
laptops.push(
    new Laptop(
        "./laptop3.jpeg",
        "Asus VivoBook 14\" bærbar pc",
        "Den bærbare PC-en Asus VivoBook 14 gir kraftfull hverdagsytelse for oppgaver som skriving, nettsurfing eller grunnleggende redigering. PC-en har et lett og slankt design, samt batteritid på opptil 8 timer slik at den enkelt kan brukes der du er.",
        6995,
        "Intel® Celeron® N4000\n 4 GB DDR4 RAM\n 64 GB eMMC-lagring"
    ));
laptops.push(
    new Laptop(
        "./laptop4.jpeg",
        "HP 14-dq1834 14\" bærbar PC",
        "Denne bærbare PC-en fra HP har et stilig og lett design på 1,5 kg, baklyst tastatur, samt en effektiv prosessor som gir lang batteritid. Med det kan du komfortabelt jobbe på farten eller se film på en 14\" FHD-skjerm med lyd fra doble høyttalere.",
        5995,
        "Intel® Core™ i3-1005G1-prosessor \n8 GB DDR4 RAM, 256 GB NVMe SSD\n Fingeravtrykkleser og Wi-Fi 6"
    ));
laptops.forEach(element => {
    let option = document.createElement("option");
    option.text = element.name;
    LaptopsInput.add(
        option
    );
});

// Sets selected laptop to 0 and displays it
selectedLaptop = laptops[0];
selectedLaptop.display();

// Display's another selected laptop if changed in the select box
LaptopsInput.onchange = () => {
    let name = LaptopsInput.options[LaptopsInput.selectedIndex].text;
    laptops.forEach(element => {
        if (element.name == name) {
            selectedLaptop = element;
            element.display();
        }
    });
};

// Inits the balance, loand and pay
Balance.innerText = person.balance;
Loan.innerText = person.loan;
Pay.innerText = person.pay;

// Onclick method to loan money
LoanButton.onclick = () => {
    person.doLoan();
};

// Onclick method to do work
WorkButton.onclick = () => {
    person.doWork();
};

// Onclick method to buy
BuyButton.onclick = () => {
    person.doBuy();
};

// Onclick method to transfer funds
BankButton.onclick = () => {
    person.doBank();
};

// Onclick method to view previous laptop owned
PreviousButton.onclick = () => {
    try {
        displayMyComputers(selectedMyLaptop - 1);
    }
    catch (any) {
        displayMyComputers(person.computers.length - 1);
    }
};
// Onclick method to view next laptop owned
NextButton.onclick = () => {
    try {
        displayMyComputers(selectedMyLaptop + 1);
    }
    catch (any) {
        displayMyComputers(0);
    }
};

// Onclick method to prompt user for a down payment to his loan
DownPaymentButton.onclick = () => {
    try {
        let amount = parseInt(prompt("Enter amount", 500));
        person.doPayDownLoan(amount);
    } catch (any) {
        alert("Can't pay");
    }

};



